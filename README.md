*Klasówka z programowania obiektowego*

23 kwietnia 2014 r.

W krainie Bajtolandii rozpoczyna się całotygodniowa majówka i turyści z tego kraju ruszają na
szlaki po swojej ojczyźnie.

W Bajtolandii położonych jest wiele pięknych miast. Każde miasto ma nazwę (różne miasta
mają różne nazwy), rok założenia (Bajtolandczycy są bardzo dumni ze swojej historii) oraz
liczbę mieszkańców. Niektóre miasta połączone są drogami, długość drogi między miastami jest
wyrażona w calkowitej liczbie kilometrów. Miasta, które łączy droga, nazywamy sąsiednimi.
Można przyjąć, że terytorium Bajtolandii nie obejmuje wysp i każde miasto ma przynajmniej
jednego sąsiada.

Każdego z siedmiu dni majówki rano turysta decyduje, czy chce pozostać w mieście, w którym
się znajduje, czy wyruszyć do jednego z sąsiednich miast. Bajtolandczycy to urodzeni biegacze,
w związku z czym podróż między sąsiednimi miastami nie zajmuje im wiele czasu. Po dotarciu
do wybranej miejscowości turysta zwiedza ją przez całe popołudnie.
Turysta rozpoczyna majówkę w mieście, w którym mieszka, i powraca do niego wieczorem
ostatniego dnia majówki. Powrót odbywa się przy użyciu samolotu, więc turysta nie musi się
martwić, że w swych wędrówkach zabrnie zbyt daleko.
Istnieje wiele różnych typów turystów, różniących się strategią wyboru kolejnego miasta.
Niektóre z typów turystów to:
 Turysta Losowy, który co rano wyrusza do losowego sąsiedniego miasta,
 Turysta Ciekawy, który wybiera sąsiednie miasto, którego jeszcze nigdy (przez całe
swoje życie) nie odwiedził. Jeżeli nie ma takiego, to wybiera dowolne sąsiednie z
najodleglejszych.
 Agroturysta, który wybiera sąsiednie miasto o najmniejszej liczbie mieszkańców. Jeżeli
takich miast jest kilka, to wybiera spośród nich dowolne z najdawniej założonych.
 Turysta Skoncentrowany, który ma swoje ulubione miasto. Jeżeli raz się w nim znajdzie,
to już w nim zostanie. Jeżeli wśród sąsiednich miast znajduje się jego ulubione, to do
niego wyrusza. W przeciwnym wypadku wybiera losowe (niestety nie umie zbyt dobrze
posługiwać się mapą, więc nie stosuje bardziej wyrafinowanych strategii poszukiwania).

Polecenia

1. Zaprojektuj i przedstaw w postaci schematu UML hierarchię klas opisującą sytuację z
powyższego zadania. W klasach umieść odpowiednie atrybuty i metody, oznacz
dziedziczenie, abstrakcyjność i interfejsy. Opisz wszelkie stosowane skróty notacyjne.
2. Zaimplementuj konstruktory i wszystkie metody dla wszystkich typów turystów opisanych 
w zadaniu (jak również metody innych klas, z których korzystają).
3. Zaimplementuj w Javie metodę przeprowadźMajówkę, realizującą przebieg tegorocznej
majówki. Metoda przyjmuje tablicę turystów oraz reprezentację sieci połączeń między
miastami w zaprojektowanej przez Ciebie formie. Można przyjąć, że każde miasto
udostępnia metodę zwiedź, przyjmującą jako argument turystę, której nie trzeba
implementować. Efektem wywołania tej metody ma być wykonanie wołań metody
zwiedź dla wszystkich turystów zgodnie z warunkami zadania. Wywołania powinny
wystąpić w kolejności dni majówki (najpierw zwiedzania dla wszystkich turystów z
pierwszego dnia, potem dla wszystkich turystów wywołania z drugiego dnia itp).
Kolejność wywołań metody zwiedź w obrębie jednego dnia może być dowolna. Na
koniec turyści mają znaleźć się znów w swoich miastach zamieszkania.