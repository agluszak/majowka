package io.gitlab.agluszak;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Agroturysta extends Turysta {

    public Agroturysta(Miasto miastoStartowe, String imię) {
        super(miastoStartowe, imię);
    }

    @Override
    protected Miasto wybierzMiasto(Mapa mapa) {
        List<Miasto> sąsiedzi = mapa.sąsiedzi(obecneMiasto);
        int najmniejszaLiczbaMieszkańców = Integer.MAX_VALUE;
        int najdawniejszyRokZałożenia = Integer.MAX_VALUE;

        for (Miasto miasto : sąsiedzi) {
            int liczbaMieszkańców = miasto.getLiczbaMieszkańców();
            if (liczbaMieszkańców < najmniejszaLiczbaMieszkańców) {
                najmniejszaLiczbaMieszkańców = liczbaMieszkańców;
            }
        }

        List<Miasto> najmniejszeMiasta = new ArrayList<>();
        for (Miasto miasto : sąsiedzi) {
            if (miasto.getLiczbaMieszkańców() == najmniejszaLiczbaMieszkańców) {
                najmniejszeMiasta.add(miasto);
            }
        }

        for (Miasto miasto : najmniejszeMiasta) {
            int rokZałożenia = miasto.getRokZałożenia();
            if (rokZałożenia < najdawniejszyRokZałożenia) {
                najdawniejszyRokZałożenia = rokZałożenia;
            }
        }

        for (Miasto miasto : najmniejszeMiasta) {
            if (miasto.getRokZałożenia() == najdawniejszyRokZałożenia) {
                return miasto;
            }
        }

        throw new NoSuchElementException();
    }
}
