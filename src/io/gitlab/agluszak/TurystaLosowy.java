package io.gitlab.agluszak;

import java.util.List;
import java.util.Random;

public class TurystaLosowy extends Turysta {
    private Random random;

    public TurystaLosowy(Miasto miastoStartowe, String imię) {
        super(miastoStartowe, imię);
        this.random = new Random();
    }

    @Override
    protected Miasto wybierzMiasto(Mapa mapa) {
        List<Miasto> sąsiedzi = mapa.sąsiedzi(this.obecneMiasto);
        return sąsiedzi.get(random.nextInt(sąsiedzi.size()));
    }
}
