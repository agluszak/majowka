package io.gitlab.agluszak;

import java.util.List;
import java.util.Random;

public class TurystaSkoncentrowany extends Turysta {
    private final Miasto ulubioneMiasto;
    private final Random random;

    public TurystaSkoncentrowany(Miasto miastoStartowe, String imię, Miasto ulubioneMiasto) {
        super(miastoStartowe, imię);
        this.ulubioneMiasto = ulubioneMiasto;
        this.random = new Random();
    }

    @Override
    protected Miasto wybierzMiasto(Mapa mapa) {
        if (obecneMiasto.equals(ulubioneMiasto)) {
            return ulubioneMiasto;
        }
        List<Miasto> sąsiedzi = mapa.sąsiedzi(obecneMiasto);
        if (sąsiedzi.contains(ulubioneMiasto)) {
            return ulubioneMiasto;
        } else {
            return sąsiedzi.get(random.nextInt(sąsiedzi.size()));
        }
    }
}
