package io.gitlab.agluszak;

import java.util.Objects;

public class Miasto {
    private final String nazwa;
    private final int rokZałożenia;
    private final int liczbaMieszkańców;

    public Miasto(String nazwa, int rokZałożenia, int liczbaMieszkańców) {
        this.nazwa = nazwa;
        this.rokZałożenia = rokZałożenia;
        this.liczbaMieszkańców = liczbaMieszkańców;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getLiczbaMieszkańców() {
        return liczbaMieszkańców;
    }

    public int getRokZałożenia() {
        return rokZałożenia;
    }

    public void zwiedź(Turysta turysta) {
        System.out.println(turysta.getImię() + " zwiedza miasto " + nazwa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nazwa, rokZałożenia, liczbaMieszkańców);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Miasto miasto = (Miasto) o;
        return getRokZałożenia() == miasto.getRokZałożenia() &&
                getLiczbaMieszkańców() == miasto.getLiczbaMieszkańców() &&
                Objects.equals(getNazwa(), miasto.getNazwa());
    }
}
