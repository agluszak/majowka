package io.gitlab.agluszak;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Miasto kielce = new Miasto("Kielce", 2137, 666);
        Miasto warszawa = new Miasto("Warszawa", 2005, 59999);
        Miasto rzeszów = new Miasto("Rzeszów", 500, 2);
        Miasto gdańsk = new Miasto("Gdańsk", 1000, 1000);
        Miasto więzienieLewe = new Miasto("Więzienie Lewe", 1, 1);
        Miasto więzieniePrawe = new Miasto("Więzienie Prawe", 2, 2);

        List<Miasto> odwiedzoneMiastaJacka = new ArrayList<>();
        odwiedzoneMiastaJacka.add(warszawa);

        Turysta więzień = new TurystaSkoncentrowany(więzienieLewe, "Więzień", więzieniePrawe);
        Turysta jacek = new TurystaCiekawy(kielce, "Jacek", odwiedzoneMiastaJacka);
        Turysta zbyszek = new Agroturysta(warszawa, "Zbyszek");
        Turysta mietek = new TurystaLosowy(rzeszów, "Mietek");
        Turysta koncentrat = new TurystaSkoncentrowany(gdańsk, "Koncentrat", rzeszów);

        Map<Miasto, Integer> odległościWięzieniaLewego = new HashMap<>();
        odległościWięzieniaLewego.put(więzieniePrawe, 10);
        Map<Miasto, Integer> odległościWięzieniaPrawego = new HashMap<>();
        odległościWięzieniaPrawego.put(więzienieLewe, 10);
        Map<Miasto, Integer> odległościWarszawy = new HashMap<>();
        odległościWarszawy.put(kielce, 200);
        odległościWarszawy.put(gdańsk, 300);
        Map<Miasto, Integer> odległościKielc = new HashMap<>();
        odległościKielc.put(warszawa, 200);
        odległościKielc.put(rzeszów, 500);
        odległościKielc.put(gdańsk, 1000);
        Map<Miasto, Integer> odległościRzeszowa = new HashMap<>();
        odległościRzeszowa.put(kielce, 100);
        Map<Miasto, Integer> odległościGdańska = new HashMap<>();
        odległościGdańska.put(warszawa, 666);
        odległościGdańska.put(kielce, 444);

        Map<Miasto, Map<Miasto, Integer>> odległości = new HashMap<>();
        odległości.put(więzienieLewe, odległościWięzieniaLewego);
        odległości.put(więzieniePrawe, odległościWięzieniaPrawego);
        odległości.put(warszawa, odległościWarszawy);
        odległości.put(kielce, odległościKielc);
        odległości.put(rzeszów, odległościRzeszowa);
        odległości.put(gdańsk, odległościGdańska);

        Mapa mapa = new Mapa(odległości);
        List<Turysta> turyści = Arrays.asList(więzień, jacek, zbyszek, mietek, koncentrat);

        Bajtolandia bajtolandia = new Bajtolandia(mapa, turyści);
        bajtolandia.przeprowadźMajówkę();

    }
}
