package io.gitlab.agluszak;

import java.util.List;

public class Bajtolandia {
    private final Mapa mapa;
    private final List<Turysta> turyści;

    public Bajtolandia(Mapa mapa, List<Turysta> turyści) {
        this.mapa = mapa;
        this.turyści = turyści;
    }

    void przeprowadźMajówkę() {
        System.out.println("Początek majówki");
        for (int i = 1; i <= 7; i++) {
            System.out.println("Jest dzień " + String.valueOf(i));
            for (Turysta turysta : turyści) {
                turysta.ruszajDoNastepnegoMiasta(mapa);
            }
        }
        for (Turysta turysta : turyści) {
            turysta.wracajDoDomu();
        }
        System.out.println("Koniec majówki");
    }
}
