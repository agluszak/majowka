package io.gitlab.agluszak;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Mapa {
    private final Map<Miasto, Map<Miasto, Integer>> odległości;

    public Mapa(Map<Miasto, Map<Miasto, Integer>> odległości) {
        this.odległości = odległości;
    }

    public Map<Miasto, Integer> sąsiedziZOdległościami(Miasto miasto) {
        return Collections.unmodifiableMap(odległości.get(miasto));
    }

    public List<Miasto> sąsiedzi(Miasto miasto) {
        return Collections.unmodifiableList(new ArrayList<>(odległości.get(miasto).keySet()));
    }


}
