package io.gitlab.agluszak;

public abstract class Turysta {
    private final Miasto miastoStartowe;
    private final String imię;
    protected Miasto obecneMiasto;

    public Turysta(Miasto miastoStartowe, String imię) {
        this.miastoStartowe = miastoStartowe;
        this.obecneMiasto = miastoStartowe;
        this.imię = imię;
    }

    public String getImię() {
        return this.imię;
    }

    public void ruszajDoNastepnegoMiasta(Mapa mapa) {
        System.out.println(imię + ": Jestem w " + obecneMiasto.getNazwa());
        this.obecneMiasto = wybierzMiasto(mapa);
        obecneMiasto.zwiedź(this);
    }

    protected abstract Miasto wybierzMiasto(Mapa mapa);

    public void wracajDoDomu() {
        this.obecneMiasto = miastoStartowe;
        System.out.println(imię + ": Wracam do domu: " + miastoStartowe.getNazwa());
    }
}
