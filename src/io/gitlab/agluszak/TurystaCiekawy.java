package io.gitlab.agluszak;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

public class TurystaCiekawy extends Turysta {

    private final List<Miasto> odwiedzoneMiasta;

    public TurystaCiekawy(Miasto miastoStartowe, String imię, List<Miasto> odwiedzoneMiasta) {
        super(miastoStartowe, imię);
        this.odwiedzoneMiasta = odwiedzoneMiasta;
    }

    @Override
    protected Miasto wybierzMiasto(Mapa mapa) {
        List<Miasto> sąsiedzi = mapa.sąsiedzi(obecneMiasto);
        for (Miasto sąsiednie : sąsiedzi) {
            if (!odwiedzoneMiasta.contains(sąsiednie)) {
                odwiedzoneMiasta.add(sąsiednie);
                return sąsiednie;
            }
        }
        Map<Miasto, Integer> sąsiedziZOdległościami = mapa.sąsiedziZOdległościami(obecneMiasto);
        int największaOdległość = 0;
        for (Miasto miasto : sąsiedziZOdległościami.keySet()) {
            int odległość = sąsiedziZOdległościami.get(miasto);
            if (odległość > największaOdległość) {
                największaOdległość = odległość;
            }
        }

        for (Miasto miasto : sąsiedziZOdległościami.keySet()) {
            if (sąsiedziZOdległościami.get(miasto).equals(największaOdległość)) {
                return miasto;
            }
        }

        throw new NoSuchElementException();
    }
}
